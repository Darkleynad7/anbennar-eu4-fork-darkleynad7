strong_colonial_rush = {
	colonists = 1
	global_colonial_growth = 50
	development_cost = -0.75
	land_maintenance_modifier = -0.66
	naval_maintenance_modifier = -0.66
	global_manpower_modifier = 0.25
	manpower_recovery_speed = 0.25
	land_forcelimit = 10
	land_morale = 0.3
	naval_morale = 0.3
	global_tax_income = 34	#28 allows some to do it but not all hmm
}

pioneer_rush = {
	colonists = 1
	global_colonial_growth = 50
	global_tax_income = 10
}

spawned_adventurer_boost = {
	land_maintenance_modifier = -0.8
	global_manpower_modifier = 0.5
	manpower_recovery_speed = 0.5
	land_forcelimit = 20
	land_morale = 0.5
	naval_morale = 0.15
	global_supply_limit_modifier = 1
	# migration_cooldown = -0.33
}

magic_enchantment_hysteria = {
	global_unrest = 2
}

ruler_cooldown_10yr = {
}
ruler_cooldown_5yr = {
}
ruler_cooldown_1yr = {
}

birth_of_a_new_people = {
	culture_conversion_cost = -0.5
	core_decay_on_your_own = -0.5
	enemy_core_creation = 0.25
	
	prestige = 1
}

corinite_zeal = {
	global_missionary_strength = 0.03
}

subsidized_income = {
	global_tax_income = 3
}

anbennar_a_dream_realised = {
	prestige = 1
	global_unrest = -5
}

applied_for_dukedom_modifier = {
	diplomatic_reputation = -1
}

orcish_slaves_in_colonies = {
	global_colonial_growth = 20
}
orcish_slaves_in_colonies_cooldown = {

}

#Rivers Stuff

arrag_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

gannag_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

darag_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

ynn_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

brelynn_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

lady_isobel_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

harafroy_estuary_modifier = {	#gnomish
	province_trade_power_value = 10
	picture = "estuary_icon"
}

ormam_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

vyech_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

kalavend_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

hjora_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

munasin_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

portroy_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

sornroy_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

bloodwine_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

middanroy_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

oddanroy_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

widderoy_estuary_modifier = {
	province_trade_power_value = 5 #cos its in 2 provinces
	picture = "estuary_icon"
}

dostanesck_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

alen_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

vroren_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

esmar_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

pearlywine_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

luna_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

mothers_sorrow_estuary_modifier = {
	province_trade_power_value = 2
	picture = "estuary_icon"
}

hapa_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

suran_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

sella_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

kharunyana_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

dhenbasana_estuary_modifier = {
	province_trade_power_value = 5
	picture = "estuary_icon"
}

telebei_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

hukai_estuary_modifier = {
	province_trade_power_value = 10
	picture = "estuary_icon"
}

#Trade province stuff

damespearl_toll = {
	province_trade_power_value = 10
}

damesteeth_toll = {
	province_trade_power_value = 10
}

ainway_river_toll = {
	province_trade_power_value = 10
}

ynnic_dam_modifier = {
	province_trade_power_value = 10
	local_defensiveness = 0.10
	picture = "ynnic_dam_modifier"
}

vanaans_tears = {
	province_trade_power_value = 10
	
	picture = "vanaans_tears"
}

ancient_giant_tower = {
	province_trade_power_value = 10
}

#Misc Province Stuff
calasandurs_elven_castle = {
	local_defensiveness = 0.25
}

castanorian_citadel = {
	local_defensiveness = 0.25
}

ruined_castanorian_citadel = {
	local_defensiveness = 0.1
}

rebuilding_castanorian_citadel = {
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
}

bulwark_fortifications = {
	local_defensiveness = 0.1
}

white_walls_of_castanor = {
	local_defensiveness = 0.25
	picture = white_wall_2
}

castonath_splendid_court = {
	country_admin_power = 1
	diplomatic_upkeep = 1
	max_absolutism = 10
}

castonath_grand_bazaar = {
	country_diplomatic_power = 1
	merchants = 1
	province_trade_power_value = 10
}

castonath_dragonforge = {
	country_military_power = 1
	global_regiment_cost = -0.1
	regiment_recruit_speed = -0.5
}

castonath_the_city_of_stone_modifier = {
	local_institution_spread = 0.10
	province_trade_power_value = 10
	local_development_cost = -0.05
	picture = "province_trade_power_value"
}

ruined_city = {
	development_cost = 1
}


#Greentide Modifiers

legacy_of_adventurers_modifier = {
	colonists = 1
	land_morale = 0.05
	prestige_from_land = 0.1
	development_cost = -0.1
	global_colonial_growth = 20
	colonist_placement_chance = 0.1
	build_cost = -0.10

	culture_conversion_cost = -0.75
	
	diplomatic_upkeep = -1
	administrative_efficiency = -0.5
}

legacy_of_greentide_invaders_modifier = {
	global_manpower_modifier = 0.25
	land_forcelimit_modifier = 0.1
	land_maintenance_modifier = -0.15
	land_morale = 0.05
	reinforce_cost_modifier = -0.25
	
	colonists = 1
	global_colonial_growth = 20
	colonist_placement_chance = 0.1
	
	global_institution_spread = -0.25
	reform_progress_growth = -0.25
	diplomatic_upkeep = -1
	administrative_efficiency = -0.2
	stability_cost_modifier = 0.15
}

#Countries

liberated_dragon_coast = {
	culture_conversion_cost = -0.2
	prestige = 1
}

arannese_lacemakers_guild = {
	trade_goods_size = 2.0
}

vanbury_steel_foundry = {
	trade_goods_size = 2.0
}

istralore_support_of_the_corinite_military = {
	army_tradition = 0.5
}

white_harbor_of_celmaldor = {
	naval_forcelimit = 10
	province_trade_power_modifier = 1
	ship_recruit_speed = -0.2
}

#Estates

drafted_siege_engineers = {
	siege_ability = 0.075
	defensiveness = 0.075
}

for_knowledge = {
	technology_cost = -0.1
}

adventurer_settlers = {
	colonist_placement_chance = 0.1
}

adventurers_leading_own_troops = {
	army_tradition_decay = 0.1
	infantry_power = 0.1
	movement_speed = 0.1
}	

adventurer_dominance = {
	global_autonomy = 0.07
	mercenary_discipline = -0.2
	merc_maintenance_modifier = 0.5
	administrative_efficiency = -0.33
}

mages_dominance = {
	technology_cost = 1
	global_autonomy = 0.03
	global_tax_modifier = -0.25
	all_power_cost = 0.33
}

conscripted_mages = {
	siege_ability = 0.5
	infantry_power = 1
	discipline = 0.1
	land_maintenance_modifier = 0.5
	diplomatic_reputation = -1
	global_tax_modifier = -0.33
	
}

talented_battlemage_cohort = {
	siege_ability = 0.25
	infantry_power = 0.25
	discipline = 0.05
}

peoples_mage_rejected = {
	local_unrest = 5
}

magical_spell_gone_wrong = {
	trade_goods_size = -0.5
	local_development_cost = 1
}

mage_organization_magisterium = {
	land_morale = 0.05
	imperial_authority = 0.05
	picture = "cardinal_icon"
}

mage_organization_centralized = {
	global_tax_modifier = -0.1
	land_morale = 0.05
	picture = "cardinal_icon"
}

mage_organization_decentralized = {
	global_tax_modifier = 0.05
	picture = "cardinal_icon"
}

expelled_mages = {
	prestige_decay = 0.05
	all_power_cost = 0.1
	diplomatic_reputation = -1
}

#Goblinic shamanism

fertility_cult = {
	global_tax_income = 0.05
	global_manpower_modifier = 0.05
	religion = yes
}

temple_destroyers = {
	loot_amount = 0.05
	land_morale = 0.05
	tolerance_heathen = -1
	religion = yes
}

conversion_of_temples = {
	global_missionary_strength = 0.01
	tolerance_heathen = -2
	religion = yes
}

#River Parties
river_party_aftermath = {
	prestige = 0.1
}
# Kheterata Bonuses/Maluses
khetarch_baqtkhet_ii = {
	manpower_recovery_speed = 0.1
	stability_cost_modifier = -0.1
}

khetarch_regency = {
	global_tax_modifier = -0.1
	stability_cost_modifier = 0.15
}

paid_for_khetarch_burial = {
	global_tax_modifier = -0.05
	stability_cost_modifier = 0.1
}

khetarch_amsiskhet_III = {
	global_institution_spread = 0.1
	technology_cost = -0.05
}

khetarch_shurkhet = {
	global_tax_modifier = -0.1
	yearly_absolutism = 0.5
	max_absolutism = 15
}

khetarch_sematkhet_ii = {
	land_morale = 0.1
	global_autonomy = -0.05
}

# Khetarch assassination modifiers. Not yet used, as the events aren't completed.

khetarch_assassinated = {
	stability_cost_modifier = 0.2
	global_unrest = 2
}

no_khet_in_kheterata = {
	stability_cost_modifier = 0.5
	global_unrest = 5
}

no_khet_in_government = {
	max_absolutism = 30
	yearly_absolutism = 0.5
}

khet_in_hiding = {
	stability_cost_modifier = 0.1
	global_unrest = 1
}

assassin_caught = {
	stability_cost_modifier = -0.2
	global_unrest = -2
}
pirate_legacy = {
global_ship_trade_power = 0.25
}

#Tradegoods Event Modifier

pr_great_ruin = {
	trade_goods_size = 3
}

dye_of_europe = {
	trade_goods_size = 1
}

damestear_rich_deposit = {
	trade_goods_size = 3
}

eordellonian_expedition_site = {
	trade_goods_size = 1
}

soulstones_of_isle_vroren = {
	trade_goods_size = 2
	local_development_cost = 0.5
}

growth_of_soulstone_trade = {
	trade_goods_size = 1
	local_development_cost = -0.1
}

ordal_fish_market = {
	trade_goods_size = 2.5
}

#Eordand Courts
eordand_spring_court = {
	production_efficiency = 0.05
}
eordand_summer_court = {
	naval_morale = 0.05
}
eordand_autumn_court = {
	infantry_power = 0.05
}
eordand_winter_court = {
	legitimacy = 0.5
	devotion = 0.25
	republican_tradition = 0.25
}
eordand_domandrod = {
	yearly_absolutism = 0.5
}
gawed_regent_court = {
	local_religious_conversion_resistance = 0.25
}

#Gerudia
gerudia_appeased_vassals = {
	vassal_income = -0.1
	reduced_liberty_desire = -5
	global_trade_power = -0.1
}
gerudia_empowered_revrhavn = {
	global_tax_modifier = 0.15
	global_trade_power = 0.15
}

crovis_invasion_mod = {
	land_maintenance_modifier = -0.2
	global_manpower_modifier = 0.2
	manpower_recovery_speed = 0.1
	land_forcelimit = 10
	land_morale = 0.15
	naval_morale = 0.15
	global_tax_income = 30
	reduced_liberty_desire = 50
}

crovis_vrorenmarch = {
	prestige = 1
	legitimacy = 1
	culture_conversion_cost = -0.75
	global_unrest = -5
	stability_cost_modifier = -0.15
	reduced_liberty_desire = 25
}

crovis_crushed = {
	reduced_liberty_desire = 50
	global_unrest = -5
	stability_cost_modifier = -0.15
	prestige = 1
	legitimacy = 1
}

grombar_fleet_advancements = {
	dip_tech_cost_modifier = -0.1
}

#Castanor Trials
castanortrials_castan = {
	legitimacy = 1
	prestige_decay = -0.01
	war_exhaustion = -0.02
	war_taxes_cost_modifier = -1
}
castanortrials_refused_trial = {
	legitimacy = -1
	prestige = -0.5
	global_unrest = 1
}
castanortrials_failed_alive = {
	legitimacy = -2
	prestige = -1
	global_unrest = 2
	stability_cost_modifier = 0.2
}
castanortrials_failed_dead = {
	global_unrest = 2
}

#Jadd
jaddempire_azka_sur_capital = { #Imperial Capital
	core_creation = -0.1
	years_of_nationalism = -2
}
jaddempire_religious_capital = { #Religion
	global_missionary_strength = 0.01
	land_morale = 0.1
}
jaddempire_bulwar_capital = { #Development
	global_trade_goods_size_modifier = 0.05
	development_cost = -0.1
}
jaddempire_brasan_capital = { #Trade
	trade_efficiency = 0.1
	trade_steering = 0.25
}
jaddempire_anbenncost_capital = { #Culture
	reform_progress_growth = 0.2
	expand_administration_cost = -0.5
}
jaddempire_dhenijansar_capital = { #Administration
	global_autonomy = -0.05
	state_governing_cost = -0.1
}
jaddempire_sramaya_capital = { #Logistics
	reinforce_cost_modifier = -0.1
	movement_speed = 0.1
}
jaddempire_aizt_ralare_capital = { #Union and Rule
	envoy_travel_time = -0.25
	yearly_absolutism = 1
}
jaddempire_tianlou_capital = { #Mystical
	free_adm_policy = 1
	idea_cost = -0.05
}
jaddempire_jaddanzar_capital = { #Military and glory
	siege_ability = 0.1
	prestige_decay = -0.01
}
jaddempire_court_area = {
	local_development_cost = -0.05
	local_unrest = -1
	local_build_cost = -0.05
}
jaddempire_court_seat = {
	local_development_cost = -0.1
	local_unrest = -2
	local_build_cost = -0.1
	local_tax_modifier = 0.1
	local_production_efficiency = 0.1
}

eor_monster_slayer = {
	prestige_decay = -0.01
	legitimacy = 0.25
	devotion = 0.25
	republican_tradition = 0.1
	land_morale = 0.05
}

#Orcs
gray_orc_fearsome_conqueror = {
	prestige_decay = -0.01
	vassal_income = 0.25
	reduced_liberty_desire = 50
	diplomatic_reputation = 1
}
orc_conversion_zeal = {
	global_missionary_strength = 0.10
	missionary_maintenance_cost = -0.33
}
ozarm_chadash_without_fight = {
	stability_cost_modifier = 0.33
}
ozarm_chadash_outraged_by_dishonor ={
	global_unrest = 4
	stability_cost_modifier = 0.25
}
ozarm_chadash_shift_of_power = {
	stability_cost_modifier = 0.25
	army_tradition_decay = -0.01
}
ozarm_chadash_defeated_pretender = {
	land_morale = 0.05
	army_tradition_decay = -0.01
}
orc_stopped_heir_duel = {
	prestige = -1
}
orc_ozarm_chadash_new_heir = {
	stability_cost_modifier = 0.15
	army_tradition_decay = -0.01
}
orc_heir_defeated_pretender = {
	prestige = 1
	army_tradition_decay = -0.01
}

#Rahen
porcelain_city = {
	trade_goods_size = 1
	trade_value_modifier = 0.5
	picture = "porcelain_city"
}

#The Jadd
the_jadd_divine_inspiration = {
	land_morale = 0.1
}
jaddari_religious_massacre = {
	local_manpower_modifier = -0.25
	local_tax_modifier = -0.25
	local_missionary_strength = 0.02
}
jaddari_visiting_jaddari_missionaries = {
	global_manpower_modifier = 0.1
	# global_institution_spread = 0.25
	global_missionary_strength = -0.05
}
jaddari_speading_the_truth_of_surael = {
	local_missionary_strength = 0.01
}
jaddari_burning_out_the_xhazobkult = {
	local_missionary_strength = 0.05
	local_unrest = 5
}

tluukt_receiving_zokka_refugees = {
	manpower_recovery_speed = 0.25
	land_forcelimit_modifier = 0.2
	land_maintenance_modifier = -0.2
	global_regiment_recruit_speed = -0.5
}
tluukt_the_rule_of_tluukt = {
	hostile_attrition = 2
	land_morale = 0.2
	defensiveness = 0.25
}
zokka_the_rule_of_zokka = {
	land_morale = 0.2
	core_creation = -0.25
	years_of_nationalism = -5
	diplomatic_upkeep = 1
	
}
zokka_monsters_among_the_monsters = {
	ae_impact = 0.1
}
zokka_influx_of_fresh_slaves = {
	global_trade_goods_size_modifier = 0.1
}

bayvic_goblin_quarter = {
	local_unrest = 2
	trade_goods_size = 0.5
}

jaddari_religious_massacre = {
	local_manpower_modifier = -0.25
	local_tax_modifier = -0.25
	local_missionary_strength = 0.02
}

artificier_city = {
	idea_cost = -0.05
}

harpy_roost_under_construction = {
	local_development_cost = 0.2
	local_defensiveness = -0.5
}

#Monster Colonial
monster_demonstration_of_power = {
	local_unrest = -1
}

lichdom_destroyed_phylactery = {
	diplomatic_reputation = 1
	ae_impact = -0.1
}

vampire_destroyed_coffin = {
	diplomatic_reputation = 1
	ae_impact = -0.1
}

defeated_evil_ruler = {
	diplomatic_reputation = 1
	ae_impact = -0.1
}

halfling_revolt_modifier = {
	local_unrest = 10
}

# Grombar
grombar_clans_angered = {
	local_unrest = 6
}

grombar_clans_are_restricted = {
	global_unrest = 1
	global_tax_modifier = 0.05
}

corintar_gray_orc_allies = {
	diplomatic_reputation = -1
	global_manpower_modifier = 0.1
	manpower_recovery_speed = 0.15
}

grombar_corintari_influence = {
	tolerance_own = -3
	tolerance_heathen = 2
	technology_cost = -0.05
	advisor_cost = -0.25
	global_institution_spread = 0.2
	embracement_cost = -0.2
	manpower_recovery_speed = -0.2
}

grombar_escanni_half_orc_migrants = {
	local_development_cost = -0.05
	local_build_cost = -0.2
}

grombar_half_orc_settlement = {
	local_colonial_growth = 25
}

grombar_religious_violence = {
	local_unrest = 12
	local_missionary_strength = -0.1
}

grombar_conversion_zeal_humanist = {
	missionaries = 1
	global_missionary_strength = 0.1
	missionary_maintenance_cost = -0.33
}

grombar_dont_die_to_converion = {
	missionaries = 1
	missionary_maintenance_cost = -0.33
}

grombar_conversion_zeal = {
	tolerance_own = 3
	tolerance_heretic = -1
	tolerance_heathen = -1
	global_missionary_strength = 0.1
	missionary_maintenance_cost = -0.33
}

grombar_weak_religion = {
	religious_unity = -0.5
	tolerance_heretic = -1
	tolerance_heathen = -1
	global_missionary_strength = -0.01
}

grombar_religious_migration = {
	local_missionary_strength = -0.25
}

grombar_edict_of_garok = {
	tolerance_own = -1
	tolerance_heretic = 2
	tolerance_heathen = 2
	religious_unity = 0.25
	stability_cost_modifier = -0.1
}

grombar_strength_of_our_faith = {
	church_power_modifier = 0.25
	tolerance_heathen = 3
}

grombar_example_for_all_of_us = {
	shock_damage = 0.1
}

grombar_coward_ruler = {
	land_morale = -0.1
}

icon_of_corin_good = {
	land_morale = 0.05
}

icon_of_corin_bad = {
	land_morale = -0.05
}

constant_rains = {
	local_development_cost = 0.20
	local_tax_modifier = -0.5
	trade_goods_size_modifier = -0.5
	local_monthly_devastation = 0.1
}

crimson_deluge_province = {
	local_monthly_devastation = 0.15
	local_unrest = 2
}

crimson_deluge_province_heavy = {
	local_monthly_devastation = 0.35
	local_unrest = 2
}

irrliam_first_ruler = {
	stability_cost_modifier = -0.15
	vassal_income = 0.15	
	diplomatic_reputation = 1
	unjustified_demands = 0.5
	prestige = 0.5
}

corintar_lothane_bluetusk = {
	army_tradition = 1
	native_assimilation = 0.5
	monthly_militarized_society = 0.15
}

corintar_bluetusk_funeral = {
	global_trade_goods_size_modifier = -0.2
}

blademarches_honorable_leader = {
	diplomatic_reputation = 1
	improve_relation_modifier = 0.1
}

blademarches_well_liked_leader = {
	ae_impact = -0.2
}

building_ruin_cliff_passage = {
	local_state_maintenance_modifier = 1
	province_trade_power_value = 5
}

wine_lord_wine = {
	local_production_efficiency = 0.75
}

lauren_sil_place_modifier_1 = {
	land_morale = 0.05
	movement_speed = 0.10
	native_uprising_chance = -0.10
}

lauren_sil_place_modifier_2 = {
	all_power_cost = -0.01
	native_uprising_chance = -0.75
	global_unrest = -2
	ae_impact = 0.25
}

lauren_sil_place_modifier_3 = {
	diplomatic_reputation = 2
	improve_relation_modifier = 0.20
}

winston_foolfoot_modifier = {
	idea_cost = -0.10
}

aucan_sil_versil_modifier = {
	tolerance_own = 2
}

dominic_of_gallowpeak_modifier_1 = {
	tolerance_own = 2
}

dominic_of_gallowpeak_modifier_2 = {
	ae_impact = -0.05
	land_morale = 0.10
}

dominic_of_gallowpeak_modifier_3 = {
	tolerance_own = 2
	land_morale = 0.05
}

dominic_of_gallowpeak_modifier_4 = {
	global_missionary_strength = 0.02
}

cecil_sapphire_eye_modifier = {
	diplomatic_reputation = 1
	legitimacy = 1
	republican_tradition = 0.3
	devotion = 1
}

nesterin_the_gaunt_modifier = {
	shock_damage = 0.10
}

iacobb_creekwhistle_modifier_1 = {
	infantry_fire = 0.3
}

iacobb_creekwhistle_modifier_2 = {
	technology_cost = -0.05
}

iacobb_creekwhistle_modifier_3 = {
	prestige = 1
	diplomatic_reputation = 1
}

iacobb_creekwhistle_modifier_4 = {
	global_autonomy = -0.01
}

iacobbs_riffles_modifier_1 = {
	fire_damage = 0.10
}

iacobbs_riffles_modifier_2 = {
	technology_cost = -0.025
}

rethag_ironbrow_modifier = {
	country_military_power = 1
}

aelnar_flow_of_money_army = {
	land_maintenance_modifier = 0.2
	land_morale = 0.05
}

aelnar_flow_of_money_priest = {
	missionaries = 1
	global_missionary_strength = 0.01
	missionary_maintenance_cost = 0.33
}

aelnar_flow_of_money_people = {
	state_maintenance_modifier = 0.33
	global_unrest = -0.5
}

aelnar_flow_of_money_bureaucracy = {
	global_tax_modifier = -0.2
	administrative_efficiency = 0.025
}

aelnar_position_of_power_army = {
	free_leader_pool = 1
	leader_cost = 0.25
}

aelnar_position_of_power_priest = {
	tolerance_own = 1
	tolerance_heretic = -1
	tolerance_heathen = -1
}

aelnar_position_of_power_people = {
	idea_cost = -0.05
	global_manpower_modifier = -0.1
	global_tax_modifier = -0.1
}

aelnar_position_of_power_bureaucracy = {
	governing_capacity_modifier = 0.33
	years_of_nationalism = 5
}

aelnar_the_goal_of_our_state_army = {
	land_forcelimit_modifier = 0.05
}

aelnar_the_goal_of_our_state_priest = {
	religious_unity = 0.1
}

aelnar_the_goal_of_our_state_people = {
	same_culture_advisor_cost = -0.1
}

aelnar_the_goal_of_our_state_bureaucracy = {
	global_autonomy = -0.01
}

aelnar_kayd_revolt = {
	land_forcelimit = 25
	naval_forcelimit = 20
	land_maintenance_modifier = -0.75
	naval_maintenance_modifier = -0.75
	infantry_power = 0.1
}

aelnar_aelthanas_revolt = {
	land_forcelimit = 10
	naval_forcelimit = 20
	land_morale = 0.1
	land_maintenance_modifier = -0.75
	naval_maintenance_modifier = -0.75
}

aelnar_sicrheior_revolt = {
	land_forcelimit = 35
	naval_forcelimit = 20
	discipline = 0.025
	land_maintenance_modifier = -0.75
	naval_maintenance_modifier = -0.75
}

aelnar_dahvar_revolt = {
	land_forcelimit = 15
	naval_forcelimit = 20
	land_morale = 0.05
	land_maintenance_modifier = -0.75
	naval_maintenance_modifier = -0.75
}

aelnar_lithiel_revolt = {
	land_forcelimit = 40
	naval_forcelimit = 20
	land_morale = 0.1
	discipline = 0.05
	land_maintenance_modifier = -0.75
	naval_maintenance_modifier = -0.75
	may_recruit_female_generals = yes
}

kayd_in_prison = {
	stability_cost_modifier = -0.1
}

sicrheior_in_prison = {
	stability_cost_modifier = -0.1
}

dahvar_in_prison = {
	stability_cost_modifier = -0.1
}

aelthanas_in_prison = {
	stability_cost_modifier = -0.1
}

long_lived_theocracy_no_abdication = {
	devotion = -1
	technology_cost = 0.1
	idea_cost = 0.1
}

# Verkal Dromak I25
I25_allow_sleepers_1 = {
	global_unrest = -1
}
I25_disallow_sleepers_1 = {
	global_unrest = 1
	mages_influence_modifier = 0.10
}
I25_disallow_sleepers_2 = {
	global_unrest = 5
	mages_influence_modifier = 0.10
}
I25_allow_sleepers_3 = {
	mages_loyalty_modifier = -0.20
}
I25_disallow_sleepers_3 = {
	mages_influence_modifier = 0.10
	mages_loyalty_modifier = 0.10
}
I25_allow_sleepers_4 = {
	mages_loyalty_modifier = -0.30
}
I25_disallow_sleepers_4 = {
	global_unrest = 3
	mages_influence_modifier = 0.10
}
I25_allow_sleepers_5 = {
	mages_loyalty_modifier = -0.20
}
I25_mages_gain_influence = {
	mages_influence_modifier = 0.40
	nobles_influence_modifier = -0.20
	nobles_loyalty_modifier = -0.10
	church_influence_modifier = -0.20
	church_loyalty_modifier = -0.10
}
I25_epic_battle = {
	global_manpower_modifier = -0.20
	land_morale = -0.2
}
I25_mages_enforced_demands = {
	mages_influence_modifier = 0.2
	technology_cost = 0.1
}

I25_ruler_killed_estates_debuff = {
	church_loyalty_modifier = -0.10
	nobles_loyalty_modifier = -0.10
	burghers_loyalty_modifier =  -0.10
}
I25_ruler_killed_no_investigation = {
	yearly_corruption = 0.3
	discipline = -0.05
}
I25_ruler_killed_investigator_disappeared = {
	global_unrest = 2
	land_morale = -0.1
}

I25_kraken_maps = {
	burghers_influence_modifier = 0.2
}
I25_kraken_magical_protection = {
	mages_influence_modifier = 0.2
}
I25_kraken_knowledge = {
	adventurers_influence_modifier = 0.2
}
I25_kraken_training = {
	nobles_influence_modifier = 0.2
}
I25_kraken_blessing = {
	church_influence_modifier = 0.2
}
I25_kraken_digging = {
	burghers_influence_modifier = 0.2
}
I25_kraken_truth = {
	land_morale = -0.1
}
I25_kraken_got_skin = {
	land_morale = 0.1
	technology_cost = -0.1
}
I25_kraken_killed_no_proof = {
	land_morale = 0.05
}
I25_kraken_lost = {
	global_unrest = 0.5
}
I25_kraken_lied = {
	discipline = -0.05
}
I25_expedition_not_sent = {
	land_morale = -0.05
}
I25_adventures_killed_kraken = {
	land_morale = 0.1
	technology_cost = -0.1
}
I25_adventurers_losing_support = {
	adventurers_influence_modifier = -0.1
}
I25_injured_monarch = {
	country_admin_power = -1
	country_diplomatic_power = -1
	country_military_power = -1
	prestige = -0.5
}
I25_kill_mages_1 = {
	mages_influence_modifier = -0.1
}
I25_kill_mages_2 = {
	mages_influence_modifier = -0.2
}
I25_kill_mages_3 = {
	mages_influence_modifier = -0.3
	all_power_cost = 0.05
}
I25_kill_mages_4 = {
	mages_influence_modifier = -0.4
	all_power_cost = 0.05
}
I25_kill_mages_5 = {
	mages_influence_modifier = -0.5
	all_power_cost = 0.1
}
I25_kill_mages_6 = {
	mages_influence_modifier = -0.6
	all_power_cost = 0.1
}
I25_kill_mages_7 = {
	mages_influence_modifier = -0.7
	all_power_cost = 0.15
}
I25_kill_mages_8 = {
	mages_influence_modifier = -0.8
	all_power_cost = 0.15
}
I25_kill_mages_9 = {
	mages_influence_modifier = -0.9
	all_power_cost = 0.2
}
I25_kill_mages_10 = {
	mages_influence_modifier = -1
	all_power_cost = 0.2
}

I25_dream_morale_up = {
	land_morale = 0.15
}
I25_dream_morale_down = {
	land_morale = -0.15
}
I25_dream_unrest_up = {
	global_unrest = 1.5
}
I25_dream_unrest_down = {
	global_unrest = -1.5
}
I25_dream_tax_up = {
	global_tax_modifier = 0.3
}
I25_dream_tax_down = {
	global_tax_modifier = -0.3
}
I25_dream_relations_up = {
	improve_relation_modifier = 0.3
}
I25_dream_relations_down = {
	improve_relation_modifier = -0.3
}

# REVERIA
reveria_thought_tolerance_campaign = {
	tolerance_heathen = 2
}

reveria_gnomish_innovations = {
	idea_cost = -0.1
}

reveria_westward_winds = {
	range = +0.2
}

reveria_discover_aelantir_modifier = {
	global_colonial_growth = 20
	colonist_placement_chance = 0.05
}

reveria_naval_enthusiasm = {
	naval_morale = 0.1
}

reveria_ready_to_fight_a = {
    land_morale = 0.05
	land_attrition = -0.20
}

reveria_protected_trade = {
	global_own_trade_power = 0.15
	trade_efficiency = 0.1
	ship_power_propagation = 0.1
}

reveria_colonial_venture = {
    colonist_placement_chance = 0.1
	global_colonial_growth = 15
}

reveria_reavers_of_the_Dameshead_sea = {
	allowed_marine_fraction = 0.1
	galley_power = 0.1
}

reveria_reavers_of_the_Divenhal_sea = {
	allowed_marine_fraction = 0.1
	galley_power = 0.1
}

reveria_second_age_of_reavers = {
	navy_tradition = 1
	navy_tradition_decay = -0.01
	naval_morale = 0.1
}

reveria_gnomish_navy = {
	flagship_cost = 1
	naval_maintenance_flagship_modifier = 1
	flagship_durability = 1
	flagship_morale = 0.20
	heavy_ship_power = 0.1
	heavy_ship_cost = 0.05
}

reveria_gnomish_enterprises = {
	development_cost = -0.1
}

reveria_gnomish_army = {
		artillery_power = 0.15
		artillery_bonus_vs_fort = 1
		artillery_cost = 0.15
		fire_damage = 0.05
		infantry_cost = 0.05
}

reveria_disrespects_host = {
		diplomatic_reputation = -2
}

reveria_recently_attended_feast = {
		prestige = 1
}

reveria_recently_hosted_feast = {
		prestige = 1.5
}

reveria_gerudian_tolerance = {
	tolerance_heathen = 1
	religious_unity = 0.10
}

reveria_playing_our_cards = {
	land_morale = 0.05
	manpower_recovery_speed = 0.1
		land_forcelimit = 2
}

reveria_ruined1 = {
	privateer_efficiency = 0.05
}

reveria_ruined2 = {
	privateer_efficiency = 0.05
	allowed_marine_fraction = 0.05
}

reveria_ruined3 = {
	privateer_efficiency = 0.1
	allowed_marine_fraction = 0.05
}

reveria_ruined4 = {
	privateer_efficiency = 0.1
	allowed_marine_fraction = 0.1
}

reveria_crown1 = {
	prestige = 0.2
	legitimacy = 0.2
}

reveria_crown2 = {
	prestige = 0.3
	legitimacy = 0.5
}

reveria_crown3 = {
	prestige = 0.5
	legitimacy = 0.7
}

reveria_preparing_new_capital = {
	local_development_cost = -0.2
}

reveria_mass_exodus = {
	local_production_efficiency = -0.3
	local_tax_modifier = -0.3
}

reveria_developing_countryside = {
	local_development_cost = -0.2
}

reveria_developed_countryside = {
	production_efficiency = 0.15
}

reveria_gnomish_administrator_tools = {
	administrative_efficiency = 0.05
}

reveria_halfling_wars = {
	manpower_recovery_speed = 0.10
	land_morale = 0.05
}

reveria_golden_dome = {
	prestige = 0.5
	heir_chance = 0.1
	monarch_lifespan = 0.1
}

reveria_empire = {
	num_accepted_cultures = 2
	diplomatic_annexation_cost = -0.15
	accept_vassalization_reasons = 5
}

reveria_restructuring_government = {
	administrative_efficiency = -0.1
}

reveria_new_state_religion = {
	missionaries = 2
	global_missionary_strength = 0.03
	tolerance_heathen = -2
}

reveria_dragon_furnace = {
		global_regiment_cost = -0.025
		trade_goods_size = 1
}

reveria_halfling_merc_adv = {
	mercenary_cost = -0.10
	merc_maintenance_modifier = -0.10
}

reveria_reverian_boatsmiths = {
	trade_efficiency = 0.1
	trade_steering = 0.1
	galley_cost = -0.1
	light_ship_cost = -0.1	
}