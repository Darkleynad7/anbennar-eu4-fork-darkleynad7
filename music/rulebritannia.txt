# Rule Britannia Music Pack 
# Songs to listen to while playing in the British Isles
# Play for Alenics

song = {
	name = "Alba"
		
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { capital_scope = { continent = europe } }
			NOT = { culture_group = alenic } #Colonies still get them
		}
		modifier = {
			factor = 2
			OR = {
				culture_group = alenic
			}
		}
	}
}

song = {
	name = "A_Battle_in_the_Highlands"
		
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { capital_scope = { continent = europe } }
			NOT = { culture_group = alenic } #Colonies still get them
		}
		modifier = {
			factor = 2
			OR = {
				culture_group = alenic
			}
		}
	}
}

song = {
	name = "Piper_Lead_Your_Clansmen"
		
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { capital_scope = { continent = europe } }
			NOT = { culture_group = alenic } #Colonies still get them
		}
		modifier = {
			factor = 2
			OR = {
				culture_group = alenic
			}
		}
	}
}
