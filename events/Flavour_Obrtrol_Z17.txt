namespace = flavor_obrtrol
# trading with the forest trolls
country_event = {
	id = flavor_obrtrol.1
	title = flavor_obrtrol.1.t
	desc = flavor_obrtrol.1.d
	picture = TRADEGOODS_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	#have as many as you like!
	option = {
		name = flavor_obrtrol.1.a
		ai_chance = { 
			factor = 100
			modifier = {
				factor = 0
			}
		}
		967 = {
			remove_human_minority_size_effect = yes
			change_culture = fjord_troll
			change_religion = ROOT
			add_devastation = 20
		}
		968 = {
			remove_human_minority_size_effect = yes
			change_culture = fjord_troll
			change_religion = ROOT
			add_devastation = 20
		}
		970 = {
			remove_human_minority_size_effect = yes
			change_culture = fjord_troll
			change_religion = ROOT
			add_devastation = 20
		}
		971 = {
			remove_human_minority_size_effect = yes
			change_culture = fjord_troll
			change_religion = ROOT
			add_devastation = 20
		}		
		add_country_modifier = {
			name = obrtrol_troll_deal_accepted
			Duration = 18250 #50 years
		}
		medium_decrease_of_human_tolerance_effect = yes
	}
	#we will not sell the humans
	option = {
		name = flavor_obrtrol.1.b
		ai_chance = {
			factor = 100
			modifier = {
				factor = 0
			}
		}
		add_country_modifier = {
			name = obrtrol_troll_deal_declined
			Duration = 18250 #50 years
		}
		medium_increase_of_human_tolerance_effect = yes		
	}
}