namespace = escanni_wars

country_event = {
	id = escanni_wars.1
	title = "escanni_wars.1.t"
	picture = {	
		trigger = {
			has_dlc = "Emperor"
		}
		picture = ITALIAN_wars_eventPicture
	}
	picture = {	
		trigger = {
			NOT = { has_dlc = "Emperor" }
		}
		picture = MILITARY_CAMP_eventPicture
	}
	desc = "escanni_wars.1.d"
	
	major = yes
	fire_only_once = yes
	
	trigger = {
		current_age = age_of_absolutism
		NOT = { has_global_flag = escanni_wars_bypass }
		OR = {
			culture_group = escanni
			primary_culture = marrodic
			primary_culture = iron_dwarf
			primary_culture = newfoot_halfling
		}
		capital_scope = {
			OR  = {
				region = south_castanor_region
				region = west_castanor_region
				region = inner_castanor_region
				area = cursewood_area
				area = whistlevale_area
			}
		}
		total_development = 150
		is_subject = no
		any_known_country = {
			OR = {
				culture_group = escanni
				primary_culture = marrodic
				primary_culture = iron_dwarf
				primary_culture = newfoot_halfling
			}
			capital_scope = {
				OR  = {
					region = south_castanor_region
					region = west_castanor_region
					region = inner_castanor_region
					area = cursewood_area
					area = whistlevale_area
				}
			}
			total_development = 150
			is_subject = no
		}
	}
	
	mean_time_to_happen = {
		months = 3
	}
	
	option = {
		name = escanni_wars.1.a
		
		custom_tooltip = escanni_wars_tt
		set_global_flag = escanni_wars_global_flag
	}
}

# End of the Escanni Wars of Consolidation
country_event = {
	id = escanni_wars.2
	title = "escanni_wars.2.t"
	picture = {	
		trigger = {
			has_dlc = "Emperor"
		}
		picture = ITALIAN_wars_eventPicture
	}
	picture = {	
		trigger = {
			NOT = { has_dlc = "Emperor" }
		}
		picture = MILITARY_CAMP_eventPicture
	}
	desc = {
		trigger = { has_saved_event_target = escanni_wars_victor }
		desc = escanni_wars.2.d
	}
	desc = {
		trigger = { NOT = { has_saved_event_target = escanni_wars_victor } }
		desc = escanni_wars.2.dunclear
	}
	
	major = yes
	
	fire_only_once = yes
	
	trigger = {
		OR = {
			culture_group = escanni
			primary_culture = marrodic
			primary_culture = iron_dwarf
			primary_culture = newfoot_halfling
		}
		capital_scope = {
			OR  = {
				region = south_castanor_region
				region = west_castanor_region
				region = inner_castanor_region
				area = cursewood_area
				area = whistlevale_area
			}
		}
		had_global_flag = {
			flag = escanni_wars_global_flag
			days = 18250
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	immediate = {
		if = {
			limit = {
				OR = {
					culture_group = escanni
					primary_culture = marrodic
					primary_culture = iron_dwarf
					primary_culture = newfoot_halfling
				}
				capital_scope = {
					OR  = {
						region = south_castanor_region
						region = west_castanor_region
						region = inner_castanor_region
						area = cursewood_area
						area = whistlevale_area
					}
				}
				is_subject = no
				num_of_owned_provinces_with = {
					value = 100
					
					OR = {
						region = south_castanor_region
						region = west_castanor_region
						region = inner_castanor_region
						area = cursewood_area
						area = whistlevale_area
					}
				}
				#Own Castonath
				owns = 831
				owns = 832
				owns = 833
			}
			save_event_target_as = escanni_wars_victor
		}
		else = {
			random_country = {
				limit = {
					OR = {
						culture_group = escanni
						primary_culture = marrodic
						primary_culture = iron_dwarf
						primary_culture = newfoot_halfling
					}
					capital_scope = {
						OR  = {
							region = south_castanor_region
							region = west_castanor_region
							region = inner_castanor_region
							area = cursewood_area
							area = whistlevale_area
						}
					}
					is_subject = no
					num_of_owned_provinces_with = {
						value = 100
						
						OR = {
							region = south_castanor_region
							region = west_castanor_region
							region = inner_castanor_region
							area = cursewood_area
							area = whistlevale_area
						}
					}
					#Own Castonath
					owns = 831
					owns = 832
					owns = 833
				}
				save_event_target_as = escanni_wars_victor
			}
		}
	}
	
	option = {
		name = escanni_wars.2.a
		
		custom_tooltip = escanni_wars_end_tt
		clr_global_flag = escanni_wars_global_flag
		
		event_target:escanni_wars_victor = {
			country_event = { id = escanni_wars.10 days = 1 }
		}
	}
}

#Escanni Wars Bypass
country_event = {
	id = escanni_wars.3
	title = "escanni_wars.3.t"
	picture = {	
		trigger = {
			has_dlc = "Emperor"
		}
		picture = ITALIAN_wars_eventPicture
	}
	picture = {	
		trigger = {
			NOT = { has_dlc = "Emperor" }
		}
		picture = MILITARY_CAMP_eventPicture
	}
	desc = "escanni_wars.3.d"
	
	major = yes
	fire_only_once = yes
	
	trigger = {
		current_age = age_of_absolutism
		NOT = { has_global_flag = escanni_wars_global_flag }
		is_year = 1640
		NOT = { is_year = 1650 }
		OR = {
			culture_group = escanni
			primary_culture = marrodic
			primary_culture = iron_dwarf
			primary_culture = newfoot_halfling
		}
		capital_scope = {
			OR  = {
				region = south_castanor_region
				region = west_castanor_region
				region = inner_castanor_region
				area = cursewood_area
				area = whistlevale_area
			}
		}
		is_subject = no
		num_of_owned_provinces_with = {
			value = 100
			
			OR = {
				region = south_castanor_region
				region = west_castanor_region
				region = inner_castanor_region
				area = cursewood_area
				area = whistlevale_area
			}
		}
		#Own Castonath
		owns = 831
		owns = 832
		owns = 833
	}
	
	mean_time_to_happen = {
		months = 3
	}
	
	option = {
		name = escanni_wars.3.a
		
		set_global_flag = escanni_wars_bypass
		country_event = { id = escanni_wars.10 days = 1 }
	}
}

# Escanni Wars winner reward
country_event = {
	id = escanni_wars.10
	title = "escanni_wars.10.t"
	picture = {	
		trigger = {
			has_dlc = "Emperor"
		}
		picture = ITALIAN_wars_eventPicture
	}
	picture = {	
		trigger = {
			NOT = { has_dlc = "Emperor" }
		}
		picture = MILITARY_CAMP_eventPicture
	}
	desc = {
		trigger = { NOT = { has_global_flag = escanni_wars_bypass } }
		desc = escanni_wars.10.d
	}
	desc = {
		trigger = { has_global_flag = escanni_wars_bypass }
		desc = escanni_wars.10.dbypass
	}
	
	major = yes
	is_triggered_only = yes
	
	option = { #Empire of Anbennar: 11-19
		name = escanni_wars.10.a
		trigger = {
			religion_group = cannorian
			government = monarchy
			hre_size = 1
			is_emperor = no
		}
		
		custom_tooltip = usurp_emperorship_choice_tt
		set_country_flag = escanni_wars_usurp_emperorship
	}
	
	option = { #Castanor: 20-29
		name = escanni_wars.10.b
		trigger = {
			has_estate = estate_castonath_patricians
			840 = {
				has_province_modifier = castanorian_citadel
				owned_by = ROOT
				is_core = ROOT
			}
			OR = {
				culture_group = escanni
				culture_group = dostanorian_g
			}
			NOT = { has_country_flag = formed_castanor_flag }
			NOT = { has_reform = adventurer_reform }
			NOT = { exists = B32 } #Castanor doesn't exist
			#NOT = { exists = Z34 } #Black Castanor doesn't exist
			NOT = { tag = B33 } #Blademarches hate Castanor
			NOT = { tag = Z34 }	#Black Castanor cannot into Castanor
			NOT = { has_country_flag = orc_nation_formed }	#prevents orc formables from forming it
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
			normal_or_historical_nations = yes
		}
		country_event = { id = escanni_wars.20 days = 30}
	}
	
	option = { #Infernal Court: 30-39
		name = escanni_wars.10.c
		trigger = {
			always = no
		}
		
	}
	
	option = { #Black Demesne: 40-49
		name = escanni_wars.10.e
		trigger = {
			always = no
		}
		
	}
	
	option = { #Generic Conquest
		name = escanni_wars.10.y
		
		add_country_modifier = {
			name = escanni_wars_escanni_imperialism
			duration = -1
		}
	}
	
	option = { #Generic Peace
		name = escanni_wars.10.z
		
		add_country_modifier = {
			name = escanni_wars_escanni_peace
			duration = -1
		}
	}
}

# Empire of Anbennar: 11-19 #
# Emperorship Usurped
country_event = {
	id = escanni_wars.11
	title = "escanni_wars.11.t"
	picture = HRE_eventPicture
	desc = "escanni_wars.11.d"
	
	major = yes
	is_triggered_only = yes
	
	# immediate = {
		# hidden_effect = { make_emperor = yes } #For some reason having it in immediate sometimes causes the EoA to be disbanded when fired by the peace treaty but not by console when fired in the same situation
	# }
	
	option = {
		name = escanni_wars.11.a
		
		make_emperor = yes
		if = {
			limit = { NOT = { hre_reform_passed = emperor_erbkaisertum } }
			hre_inheritable = yes
			set_country_flag = usurp_emperorship_hereditary_flag
		}
		add_imperial_influence = 100
		add_country_modifier = {
			name = escanni_wars_escanni_emperor
			duration = -1
		}
	}
}

# Electors called into war
country_event = {
	id = escanni_wars.12
	title = "escanni_wars.12.t"
	picture = HRE_eventPicture
	desc = "escanni_wars.12.d"
	
	is_triggered_only = yes
	
	option = {
		name = escanni_wars.12.a
		ai_chance = { factor = 100 }
		
		add_prestige = 25
		join_all_defensive_wars_of = emperor
	}
	
	option = {
		name = escanni_wars.12.b
		ai_chance = { factor = 0 }
		
		add_prestige = -25
		add_legitimacy = -25
	}
}

# Castanor: 20-29 #
# A walk through Pantheonway + reflections on history of Castanor
country_event = {
	id = escanni_wars.20
	title = escanni_wars.20.t
	picture = 10_Jain_Estate_eventPicture
	desc = {
		trigger = { NOT = { has_global_flag = escanni_wars_bypass } }
		desc = escanni_wars.20.descWar
	}
	desc = {
		trigger = { has_global_flag = escanni_wars_bypass }
		desc = escanni_wars.20.descBypass
	}
	
	is_triggered_only = yes
	
	option = {
		name = escanni_wars.20.a
		add_country_modifier = {
			name = escanni_wars_reflecting_on_past_castans
			duration = -1
		}
		country_event = {
			id = escanni_wars.21
			days = 180
			random = 60
		}
	}
}
# Discovery that the Trials are still legit - pay some money and time to restore Trialmount so that the Trials can be taken again
country_event = {
	id = escanni_wars.21
	title = escanni_wars.21.t
	picture = COMET_SIGHTED_eventPicture
	desc = escanni_wars.21.desc
	
	is_triggered_only = yes
	
	option = {
		name = escanni_wars.21.a
		country_event = {
			id = escanni_wars.22
			days = 365
			random = 365
		}
		add_years_of_income = -0.5
	}
}
# Patricians object to your intentions to take the trials - this sets up later disaster
country_event = {
	id = escanni_wars.22
	title = escanni_wars.22.t
	picture = OPRICHINA_eventPicture
	desc = escanni_wars.22.desc
	
	is_triggered_only = yes
	
	option = {
		name = escanni_wars.22.a
		add_estate_loyalty = {
			estate = estate_castonath_patricians
			loyalty = -5
		}
		set_country_flag = escanni_wars_castanor_option_unlocked
		set_country_flag = first_time_trying_trials
		custom_tooltip = escanni_wars_form_castanor_unlocked_tt
		remove_country_modifier = escanni_wars_reflecting_on_past_castans
	}
}
# Infernal Court: 30-39 #

# Black Demesne: 40-49 #
